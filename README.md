Hi, my name is Marc a.k.a. schrieveslaach. I'm a Senior Software Architect at [aixigo AG](https://www.aixigo.com/) and I'm a Open Source Enthusiast.

You can follow me on <a href="https://fosstodon.org/@schrieveslaach" rel="me">Mastodon</a> or you can visit my webpage [schrieveslaach.de](https://schrieveslaach.de) (only in German, unfortunately).
